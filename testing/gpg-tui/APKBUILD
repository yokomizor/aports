# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=gpg-tui
pkgver=0.9.3
pkgrel=0
pkgdesc="Terminal user interface for GnuPG"
url="https://github.com/orhun/gpg-tui"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
makedepends="cargo gpgme-dev libxcb-dev libxkbcommon-dev libgpg-error-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/orhun/gpg-tui/archive/v$pkgver/gpg-tui-$pkgver.tar.gz"

build() {
	cargo build --release --locked

	mkdir completions
	OUT_DIR=completions cargo run --release --bin gpg-tui-completions
}

check() {
	cargo test --release --locked
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 man/$pkgname.1 -t "$pkgdir"/usr/share/man/man1/

	install -Dm644 completions/$pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 completions/$pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 completions/_$pkgname "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
f87e714872f7cc1e2daba431050a790cd56cc8d3d2382d1b24752f694f56b02d11fa1e138ee16042ef95c1ead49170836994f37dd3096141d98314978ed6eb7e  gpg-tui-0.9.3.tar.gz
"
