# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=dart-sass
pkgver=1.55.0
pkgrel=2
pkgdesc="The primary implementation of Sass"
url="https://sass-lang.com/dart-sass"
arch="x86_64"	# dart
license="MIT"
makedepends="dart dart-sdk"
source="https://github.com/sass/dart-sass/archive/refs/tags/$pkgver/dart-sass-$pkgver.tar.gz"
builddir="$srcdir/dart-sass-$pkgver/"
options="!strip" # dart compile exe can't be stripped

prepare() {
	default_prepare

	dart pub get
}

build() {
	dart compile exe -Dversion="$pkgver" bin/sass.dart
}

check() {
	# sanity
	bin/sass.exe --version
}

package() {
	install -Dm755 bin/sass.exe "$pkgdir"/usr/bin/sass
}

sha512sums="
b432fd3af60c224baac630e59ddeff228b9d293245ecc808ba3657d3ced1ca2fc18fe0df05aa215f49348acb4382df50688c1b9b0d3758b2bfd365171a8d171e  dart-sass-1.55.0.tar.gz
"
