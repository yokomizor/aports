# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.37.1
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make build VERSION=$pkgver
}

check() {
	make test
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
8c95e2c62a8c80b173096ba8f6322d77afa346d91b14798e8864acec4048ed5c2218e7821ed0f41b69f7b7b921e8c8ec1978e4a5ac6a9b20f2e6026babc7176b  trivy-0.37.1.tar.gz
"
