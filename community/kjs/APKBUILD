# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjs
pkgver=5.102.0
pkgrel=0
pkgdesc="Support for JS scripting in applications"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND BSD-3-Clause AND MIT"
depends_dev="qt5-qtbase-dev perl-dev pcre-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjs-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
6dc43c4cbe5c79dd1622e0147d70b3de2c20960961617f04bfa9a4c5d4aacc785d1d9e8134dba9fe52b04b7460fdcd2be658dee62f5e4c6d6a08e753610940a7  kjs-5.102.0.tar.xz
"
