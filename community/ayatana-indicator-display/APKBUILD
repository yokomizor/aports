# Contributor: Luca Weiss <luca@z3ntu.xyz>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=ayatana-indicator-display
pkgver=22.9.3
pkgrel=0
pkgdesc="Ayatana Indicator Display"
url="https://github.com/AyatanaIndicators/ayatana-indicator-display"
arch="all"
license="GPL-3.0-only"
makedepends="
	cmake
	cmake-extras
	glib-dev
	intltool
	libayatana-common-dev
	libgudev-dev
	properties-cpp-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://github.com/AyatanaIndicators/ayatana-indicator-display/archive/$pkgver/ayatana-indicator-display-$pkgver.tar.gz"
options="!check" # requires unpackaged dependencies (at least libqtdbusmock & libqtdbustest)

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ad7594a54b2ff27aeedeb9a4dff6d7cbf4d7bdbeaa740162cc74afe1ba1103622d9332dfc46c980b751e276a45f099e79042842b0a39c593fc429fcdcc9ce40f  ayatana-indicator-display-22.9.3.tar.gz
"
