# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-fiona
_pkgorig=Fiona
pkgver=1.9.0
pkgrel=0
pkgdesc="Fiona reads and writes geographical data files"
url="https://github.com/Toblerity/Fiona"
arch="all"
license="BSD"
depends="
	cython
	gdal
	gdal-dev
	python3
	python3-dev
	py3-attrs
	py3-click-plugins
	py3-cligj
	py3-certifi
	py3-munch
	py3-six
	py3-tz
	"
checkdepends="py3-pytest py3-pytest-cov py3-boto3"
makedepends="py3-setuptools"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/F/Fiona/Fiona-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"
options="!check" # several tests are failing

build() {
	python3 setup.py build
}

check() {
	mv fiona backup
	# inspired by Arch package
	local python_version=$(python3 -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
	PYTHONPATH="$PWD"/build/lib.linux-$CARCH-$python_version/ pytest -vv --color=yes -m "not wheel"
	mv backup fiona
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# conflicts with fio package
	mv "$pkgdir"/usr/bin/fio "$pkgdir"/usr/bin/fiona-fio
}

sha512sums="
4c52b1b08e699d33c59e41786a98b70c1549a502ccd1e63147825fe47aec62a95f5426f9c90d0f7b57db64e974c77830767a0ff4cbc577ff50d9d895babfff54  py3-fiona-1.9.0.tar.gz
"
